---
####################### Banner #########################
banner:
  title : "A real-time operating system for microcontrollers written in Rust"
  image : "images/bern-rtos-banner.svg"

##################### Feature ##########################
feature:
  enable : true
  title : "Design Principles"
  feature_item:
    - name : "System Stability"
      icon : "fas fa-fire-extinguisher"
      content : "An embedded application should never crash. The kernel design prevents type conversion issues and has system stability in mind."
      
    - name : "Processes"
      icon : "fas fa-shield-alt"
      content : "Processes run isolated from each other. A process consists of an allocator and threads. Thus, critical process are protected from unsafe threads."

    - name : "Stackful Threads"
      icon : "fas fa-layer-group"
      content : "Every thread has its own stack. By default stacks can only be accessed within a process. Stack overflow is prevented.
      <span style=\"opacity:0;\">.......................................... .</span>"

    - name : "Flexibility"
      icon : "fas fa-pencil-ruler"
      content : "The kernel only depends on the CPU core, any/no HAL can be used. All aspects of the kernel can be configured by the user.
      <span style=\"opacity:0;\">...........</span>"

    - name : "Round-Robin Scheduling"
      icon : "fas fa-calendar-alt"
      content : "High priority tasks preempt lower ones. Tasks with the same priority run at max for one time-slice.
      <span style=\"opacity:0;\">.......................................................</span>"

    - name : "Real-Time"
      icon : "fas fa-stopwatch"
      content : "Critical Sections are kept as short as possible (atomic operation preffered). Interrupts can run without kernel interation."

######################### Service #####################
service:
  enable : true
  service_item:
    - title : "Documentation"
      images:
      - "images/bookmark-regular.svg"
      content : "The documentation is split into API documentation (generated from the code and published on docs.rs) and a user level documentation focused on concepts published as online books.<br>
      The online book documentation consists of:
      <ul>
      <li>[RTOS Analysis and Rust Introduction](https://rtos-analysis-and-rust-intro.bern-rtos.org/title_page.html)</li>
      <li>[Bern RTOS Kernel](https://kernel.bern-rtos.org)</li>
      </ul><br>
      The API documentation consists of:
      <br><br><ul>
      <li>[Bern RTOS Kernel API](https://docs.rs/bern-kernel/latest/bern_kernel/)</li>
      <li>[Bern Test: A hardware integration test framework](https://docs.rs/bern-test/latest/bern_test/)</li>
      </ul>"
        
    - title : "Software Requirements Specification"
      images:
      - "images/clipboard-regular.svg"
      content : "The software requirement specification (SRS) defines the features and behavior of the kernel. It lists all functionalities queued for impelementation and tracks their status. It also serves as an overview of the kernel and the direction the development is taking."
      button:
        enable : true
        label : "More"
        link : "https://kernel-srs.bern-rtos.org"
        
    - title : "Source Code"
      images:
      - "images/code-branch-solid.svg"
      content : "The kernel, tools and documentation are all open source.<br>
      The main repositories are:
      <ul>
      <li>[bern-kernel: Platform independent core components](https://gitlab.com/bern-rtos/bern-rtos)</li>
      <li>[bern-test: A hardware integration test framework](https://gitlab.com/bern-rtos/tools/bern-test)</li>
      <li>[Espresso machine example project](https://gitlab.com/bern-rtos/demo/espresso-machine/firmware)</li>
      </ul>"
      button:
        enable : true
        label : "More"
        link : "https://gitlab.com/bern-rtos"
        
---
